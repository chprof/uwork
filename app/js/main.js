(function(){
	var doc = $(document),
		win = $(window);

	doc.ready(function() {
		$('[data-toggle="tooltip"]').tooltip()
		$('.collapse').on('show.bs.collapse', function(e) {
			var target = $(this).attr('id');
			var img = $('img[data-id="#'+target+'"]');
			img.removeClass('d-none');
		}).on('hide.bs.collapse', function(e) {
			var target = $(this).attr('id');
			var img = $('img[data-id="#'+target+'"]');
			img.addClass('d-none');
		});
		$('[data-toggle="popover"]').popover({
			trigger: 'manual'
		}).on("mouseenter", function () {
			var _this = this;
			$(this).popover("show");
			$(".popover").on("mouseleave", function () {
				$(_this).popover('hide');
			});
		}).on("mouseleave", function () {
			var _this = this;
			setTimeout(function () {
				if (!$(".popover:hover").length) {
					$(_this).popover("hide");
				}
			}, 100);
		});
	});
	(function() {
		if ( $(window).width() < 768 ) {
			$('.sidebar').removeClass('visible');
			$('.sidebar-sub-collapse').attr('style', '');
			$('[data-clear="show"]').removeClass('show');
		} else {
			$('.sidebar').addClass('visible');
			$('.sidebar-sub-collapse').attr('style', '');
			$('body').removeClass('modal-open');
			$('[data-clear="show"]').addClass('show');
		}
		$(window).on('resize', function() {
			if ( $(window).width() < 768 ) {
				$('.sidebar').removeClass('visible');
				$('.sidebar-sub-collapse').attr('style', '');
				$('[data-clear="show"]').removeClass('show');
			} else {
				$('.sidebar').addClass('visible');
				$('.sidebar-sub-collapse').attr('style', '');
				$('body').removeClass('modal-open');
				$('[data-clear="show"]').addClass('show');
			}
		})
	}())
	$('[data-toggle="toggleVisible"]').click(function() {
		var target = $(this).data('target');
		$('[data-id="'+target+'"]').toggleClass('visible');
		if ( $(this).data('overflow-body') == 'hidden' ) {
			if ( $(window).width() < 768 ) {
				$('body').toggleClass('modal-open');
			}
		}
		if ( $(this).data('target') == 'elems' ) {
			$('[data-id="'+target+'"]').toggleClass('d-none');
		}
		if ( $(window).width() > 767 ) {
			$('[data-id="'+target+'"].sidebar-sub-collapse').slideToggle()
		}
	});
	$('[data-toggle="anchor"]').click(function(e) {
		e.preventDefault();
		var target = $(this).data('target');
		$('html,body').animate({scrollTop: $('#'+target).offset().top},'slow');
		$(this).closest('.visible').removeClass('visible');
	});
	(function() {
		var count = 0;
		var sideBarDataId = $('.sidebar').data('id');
		$('.sidebar input[type="checkbox"]').each(function() {
			if ( $(this).is(':checked') ) {
				count += 1;
			}
		});
		if ( count > 0 ) {
			$('.sidebar-sub-collapse').attr('data-id', sideBarDataId);
		}
		if ( count < 1 ) {
			$('.sidebar-sub-collapse').attr('data-id', '');
		}
		$('.sidebar input[type="checkbox"]').change(function() {
			if ( $(this).prop('checked') == true ) {
				count += 1;
			} else {
				count -= 1;
			}
			console.log(count)
			if ( count > 0 ) {
				$('.sidebar-sub-collapse').attr('data-id', sideBarDataId);
			}
			if ( count < 1 ) {
				$('.sidebar-sub-collapse').attr('data-id', '');
			}
		})
	}());
	$('.goods-carousel').owlCarousel({
		items: 4,
		nav: true,
		dots: false,
		responsive: {
			0: {
				items: 1,
			},
			576: {
				items: 2,
			},
			992: {
				items: 3,
			},
			1200: {
				items: 4,
			},
		}
	});
	if ( $('.sidebar-sticky').length ) {
		var scrollFlag = false;
		var sidebarTop = $('.sidebar-sticky').offset().top;
		$(window).on('scroll', function(e) {

			var top = $(this).scrollTop();
			if ( top > 100 ) {
				if ( !scrollFlag ) {
					$('.sidebar-sticky').removeClass('sidebar-sticky_unvisible');
					scrollFlag = true;
				}
			}
			// console.log(scrollFlag)
			if ( top < 100 ) {
				if ( scrollFlag ) {
					$('.sidebar-sticky').addClass('sidebar-sticky_unvisible');
					scrollFlag = false;
				}
			}
		})
	};
	new WOW().init();
	$('.gallery').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'a', // the selector for gallery item
			type: 'image',
			gallery: {
			  enabled:true
			}
		});
	});
	$('.sidebar-nav a').click(function(event) {
		event.preventDefault();
		$($(this).attr('href'))[0].scrollIntoView();
		// console.log($($(this).attr('href'))[0])
		scrollBy(0, -60);
		// $('.sidebar-nav a').removeClass('active');
		// $($(this).attr('href'))[0].classList.add('active');
		// if ( $($(this).attr('href'))[0].classList.contains('active') ) {
		// 	$(this).addClass('active');
		// }
	});
	$('[data-trigger="link"]').click(function(e) {
		if ( !($(e.target).hasClass('dropdown-link-toggle') || $(e.target).parent().hasClass('dropdown-link-toggle')) ) {
			var target = $(this).data('target');
			var href = $('[data-id="'+target+'"]').attr('href');
			window.location.href = href;
		}
	});
}());


