module.exports = function() {
	$.gulp.task("scripts", function() {
		return $.gulp.src("./app/js/main.js")
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
	$.gulp.task("bundle", function() {
		return $.gulp.src([
				"node_modules/popper.js/dist/umd/popper.min.js",
				"node_modules/bootstrap/dist/js/bootstrap.min.js",
				"node_modules/owl.carousel/dist/owl.carousel.min.js",
				"node_modules/owl.carousel/dist/owl.carousel.min.js",
				"node_modules/magnific-popup/dist/jquery.magnific-popup.min.js",
				"node_modules/wow.js/dist/wow.min.js"
			])
			.pipe($.gp.concat('vendor.min.js'))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
};